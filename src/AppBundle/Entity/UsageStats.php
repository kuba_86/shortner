<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="UsageStats")
 */
class UsageStats {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var usageStats[]|ArrayCollection
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Link", inversedBy="usageStats")
     * @ORM\JoinColumn(nullable=false)
     */
    private $link;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $publishAt;

    /**
     * @ORM\Column(type="string")
     */
    private $ip;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="AppBundle\Entity\Country", inversedBy="usageStats", cascade={"persist"}
     * )
     */
    private $country;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="AppBundle\Entity\Browser", cascade={"persist"}
     * )
     */
    private $browser;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="OperationSystem", cascade={"persist"}
     * )
     */
    private $operationSystem;

    public function __construct()
    {
        $this->publishAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set publishAt
     *
     * @param \DateTime $publishAt
     *
     * @return Stats
     */
    public function setPublishAt(\DateTime $publishAt) {
        $this->publishAt = $publishAt;

        return $this;
    }

    /**
     * Get publishAt
     *
     * @return \DateTime
     */
    public function getPublishAt() {
        return $this->publishAt;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Stats
     */
    public function setIp() {
        $this->ip = $_SERVER['REMOTE_ADDR'];

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp() {
        return $this->ip;
    }

    /**
     * Set link
     *
     * @param \AppBundle\Entity\Link $link
     *
     * @return Stats
     */
    public function setLink(\AppBundle\Entity\Link $link) {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return \AppBundle\Entity\Link
     */
    public function getLink() {
        return $this->link;
    }

    /**
     * Set country
     *
     * @param \AppBundle\Entity\Country $country
     *
     * @return Stats
     */
    public function setCountry(\AppBundle\Entity\Country $country = null) {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \AppBundle\Entity\Country
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set browser
     *
     * @param \AppBundle\Entity\Browser $browser
     *
     * @return Stats
     */
    public function setBrowser(\AppBundle\Entity\Browser $browser = null) {
        $this->browser = $browser;

        return $this;
    }

    /**
     * Get browser
     *
     * @return \AppBundle\Entity\Browser
     */
    public function getBrowser() {
        return $this->browser;
    }

    /**
     * Set operationSystem
     *
     * @param \AppBundle\Entity\OperationSystem $operationSystem
     *
     * @return Stats
     */
    public function setOperationSystem(\AppBundle\Entity\OperationSystem $operationSystem = null) {
        $this->operationSystem = $operationSystem;

        return $this;
    }

    /**
     * Get operationSystem
     *
     * @return \AppBundle\Entity\OperationSystem
     */
    public function getOperationSystem() {
        return $this->operationSystem;
    }


    /**
     * Add link
     *
     * @param \AppBundle\Entity\Link $link
     *
     * @return UsageStats
     */
    public function addLink(\AppBundle\Entity\Link $link)
    {
        $this->link[] = $link;

        return $this;
    }

    /**
     * Remove link
     *
     * @param \AppBundle\Entity\Link $link
     */
    public function removeLink(\AppBundle\Entity\Link $link)
    {
        $this->link->removeElement($link);
    }
    /**
     * Constructor
     */
  


    /**
     * Add country
     *
     * @param \AppBundle\Entity\Country $country
     *
     * @return UsageStats
     */
    public function addCountry(\AppBundle\Entity\Country $country)
    {
        $this->country[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param \AppBundle\Entity\Country $country
     */
    public function removeCountry(\AppBundle\Entity\Country $country)
    {
        $this->country->removeElement($country);
    }
}
