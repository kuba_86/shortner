<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="Link")
 */
class Link {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $urlHash;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Pole nie może być puste")
     * @Assert\Url(message="Podaj poprawny adres url")
     */
    private $url;

    /**
     * 
     * @ORM\OneToMany(targetEntity="UsageStats", mappedBy="link", fetch="EAGER")
     * 
     */
    private $usageStats;
   
    /**
     * Construct
     */
   

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set urlHash
     *
     * @param string $urlHash
     *
     * @return Link
     */
    public function setUrlHash()
    {
        $this->urlHash = md5($_SERVER['REMOTE_ADDR'].date(DATE_RFC822));

        return $this;
    }

    /**
     * Get urlHash
     *
     * @return string
     */
    public function getUrlHash()
    {
        return $this->urlHash;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Link
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Add usageStat
     *
     * @param \AppBundle\Entity\UsageStats $usageStat
     *
     * @return Link
     */
    public function addUsageStat(\AppBundle\Entity\UsageStats $usageStat)
    {
        $this->usageStats[] = $usageStat;

        return $this;
    }

    /**
     * Remove usageStat
     *
     * @param \AppBundle\Entity\UsageStats $usageStat
     */
    public function removeUsageStat(\AppBundle\Entity\UsageStats $usageStat)
    {
        $this->usageStats->removeElement($usageStat);
    }

    /**
     * Get usageStats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsageStats()
    {
        return $this->usageStats;
    }
}
