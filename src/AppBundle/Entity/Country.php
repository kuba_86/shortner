<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * @ORM\Entity
 * @ORM\Table(name="Country")
 * @UniqueEntity("name")
 * 
 */
class Country{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $name;
    
    /**
     * @ORM\OneToMany(targetEntity="UsageStats", mappedBy="country", fetch="EAGER")
     */
    private $usageStats;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->usageStats = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add usageStat
     *
     * @param \AppBundle\Entity\UsageStats $usageStat
     *
     * @return Country
     */
    public function addUsageStat(\AppBundle\Entity\UsageStats $usageStat)
    {
        $this->usageStats[] = $usageStat;

        return $this;
    }

    /**
     * Remove usageStat
     *
     * @param \AppBundle\Entity\UsageStats $usageStat
     */
    public function removeUsageStat(\AppBundle\Entity\UsageStats $usageStat)
    {
        $this->usageStats->removeElement($usageStat);
    }

    /**
     * Get usageStats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsageStats()
    {
        return $this->usageStats;
    }
}
