<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\UsageStats;
use AppBundle\Entity\Country;
use AppBundle\Entity\Link;
use AppBundle\Entity\Browser;
use AppBundle\Entity\OperationSystem;
use AppBundle\Form\AddLinkType;
use AppBundle\Utils\Helper;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        $form = $this->createForm(AddLinkType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $link = new Link();
            $link = $form->getData();
            $link->setUrlHash();
            $em->persist($link);
            $em->flush();
            $this->addFlash(
                    'notice', 'Link został dodany'
            );

            return $this->redirectToRoute('homepage');
        }
        return $this->render('default/index.twig', [
                    'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{url_hash}", name="showStats", requirements={"url_hash"=".*(\_)$"})
     */
    public function showStatsAction($url_hash) {

        $links = $this->getDoctrine()
                ->getRepository(Link::class)
                ->findBy([
            'urlHash' => rtrim($url_hash, '_')
        ]);

        return $this->render('default/statistics.twig', [
                    'links' => $links
        ]);
    }

    /**
     * @Route("/{url_hash}", name="redirectUrl")
     * @ParamConverter("link", options={"mapping"={"url_hash"="urlHash"}})
     */
    public function showRedirect(Link $link, Helper $helper) {

        $em = $this->getDoctrine()->getManager();
        $validator = $this->get('validator');

        $country = $helper::checkDuplicateStats(new Country(), $em, $validator, $this->getDoctrine(), Country::class, $helper::getCountry($_SERVER['REMOTE_ADDR']));
        $browser = $helper::checkDuplicateStats(new Browser(), $em, $validator, $this->getDoctrine(), Browser::class);
        $os = $helper::checkDuplicateStats(new OperationSystem, $em, $validator, $this->getDoctrine(), OperationSystem::class);

        $stats = new UsageStats();
        $stats->setLink($link);
        $stats->setIp();
        $stats->setBrowser($browser);
        $stats->setCountry($country);
        $stats->setOperationSystem($os);
        $em->persist($stats);
        $em->flush();

        return $this->redirect($link->getUrl());
    }

}

/**
 * $id_country = null;
        $id_browser = null;
        $id_os = null;
        
   
        $em = $this->getDoctrine()->getManager();

        $country = new Country();
        $country->setName();
        $validator = $this->get('validator');

        $errors = $validator->validate($country);
        
        if (count($errors) > 0) {
            $id_browser = $errors[0]->getCause('id')[0]->getId();
        } else {
            $em->persist($country);
          //  $em->flush();
        }

 */


/**
 *  $id_country = null;
            $id_browser = null;
            $id_os = null;


            $em = $this->getDoctrine()->getManager();
            $validator = $this->get('validator');

            $country = new Country();
            $country->setName();
            $errors = $validator->validate($country);

            if (count($errors) > 0) {
                $id_country = $errors[0]->getCause('id')[0]->getId();
            } else {
                $em->persist($country);
                $em->flush();
            }

            $browser = new Browser();
            $browser->setName();
            $errors = $validator->validate($browser);

            if (count($errors) > 0) {
                $id_browser = $errors[0]->getCause('id')[0]->getId();
            } else {
                $em->persist($browser);
                $em->flush();
            }
            
            $os = new OperationSystem();
            $country->setName();
            $errors = $validator->validate($os);

            if (count($errors) > 0) {
                $id_os = $errors[0]->getCause('id')[0]->getId();
            } else {
                $em->persist($os);
                $em->flush();
                $id_os = 
            }
 */
