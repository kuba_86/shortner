<?php

namespace AppBundle\Utils;

class Helper{

    public static function getCountry($ip) {
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                }
                if (filter_var($_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                }
            }
        }


        $ipdat = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        return $ipdat->geoplugin_countryName;
    }
    
    public static function checkDuplicateStats($inst, $em, $validator, $doctrine, $repository,  $nameValue = null) {
        echo get_parent_class($validator);
        $inst->setName($nameValue);
        $errors = $validator->validate($inst);

        if (count($errors) > 0) {
            $inst = $doctrine
                    ->getRepository($repository)
                    ->find($errors[0]->getCause('id')[0]->getId());
        } else {
            $em->persist($inst);
            $em->flush();
        }
        return $inst;
    }

}
